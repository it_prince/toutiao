import request from '@/utils/request'

// 获取所有频道
export const getAllChannels = (data) => {
  return request({
    method: 'GET',
    url: '/v1_0/channels',
    data
  })
}

// 增加用户频道
export const addUserChannels = (channel) => {
  return request({
    method: 'PATCH',
    url: '/v1_0/user/channels',
    data: {
      channels: [channel]
    }
  })
}

// 删除用户频道
export const deleteUserChannels = (channelId) => {
  return request({
    method: 'DELETE',
    url: `/v1_0/user/channels/${channelId}`
  })
}
