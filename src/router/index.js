import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: () => import('@/views/layout'),
    children: [
      {
        path: '',
        name: 'Home',
        component: () => import('@/views/home')
      },
      {
        path: 'video',
        name: 'Video',
        component: () => import('@/views/video')
      },
      {
        path: 'my',
        name: 'My',
        component: () => import('@/views/my')
      },
      {
        path: 'qa',
        name: 'Qa',
        component: () => import('@/views/qa')
      }
    ]
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('@/views/login')
  },
  {
    path: '/search',
    name: 'Search',
    component: () => import('@/views/search')
  },
  {
    path: '/article/:articleId',
    name: 'Article',
    component: () => import('@/views/article'),
    props: true // 将动态路径的参数映射到组件的props属性中
  },
  {
    path: '/user/profile',
    name: 'user-profile',
    component: () => import('@/views/user-profile')
  }
]

const router = new VueRouter({
  routes
})

export default router
