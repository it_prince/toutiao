import Vue from 'vue'
import Vuex from 'vuex'
// 引入 封装的 本地存储
import { getItem, setItem } from '@/utils/storage'
Vue.use(Vuex)
const TOKEN_KEY = 'TOUTIAO_KEY'

export default new Vuex.Store({
  state: {
    user: getItem(TOKEN_KEY)
  },
  mutations: {
    serUser (state, data) {
      state.user = data
      setItem(TOKEN_KEY, data)
    }
  },
  actions: {},
  modules: {}
})
