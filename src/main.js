import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// 加载全局样式  【增加这一句】
import './styles/index.less'
// 引入 vant 组件库
import Vant from 'vant'
import 'vant/lib/index.css'

// 动态设置 REM 基准值
import 'amfe-flexible'

// import "./utils/day";
// 导入时间插件
import dayjs from 'dayjs'
import relativeTime from 'dayjs/plugin/relativeTime'
// 导入语言包
import 'dayjs/locale/zh-cn'
// 使用中文语言包
dayjs.locale('zh-cn')
// 注册相对时间插件
dayjs.extend(relativeTime)
// 定义全局格式时间的过滤器
Vue.filter('relativeTime', (time) => {
  return dayjs().to(dayjs(time))
})

Vue.use(Vant)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: (h) => h(App)
}).$mount('#app')
