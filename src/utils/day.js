import dayjs from 'dayjs'

import 'dayjs/locale/zh-cn'

dayjs.locale('zh-cn')

console.log(dayjs('2019-01-25').format('DD/MM/YYYY'))
